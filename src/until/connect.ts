import mysql from 'mysql2/promise'

const sqlQuery = async () => {
  const Connection = await mysql.createConnection({
    host: '127.0.0.1',
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
  })
  // const [rows] = await Connection.query(sql)
  return Connection
}

export default sqlQuery
