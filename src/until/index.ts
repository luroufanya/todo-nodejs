import mysql from 'mysql2/promise'

async function query({ url }: { url: string }) {
  const res = await fetch(`http://localhost:3000/${url}`)
  const data = await res.json()
  return data
}

export { query }
