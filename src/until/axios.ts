const get = async ({ url }: { url: string }) => {
  const res = await fetch(`http://47.108.136.163:81/${url}`)
  return res.json()
}
const post = async ({ url, body }: { url: string; body?: any }) => {
  console.log(body)

  const res = await fetch(`http://47.108.136.163:81/${url}`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(body || {}),
  })
  return res.json()
}
const webQuery = {
  get,
  post,
}
export default webQuery
