import sqlQuery from '../../until/connect'

export default async function add(req: any, res: any) {
  try {
    const { value, key, status } = req?.body
    console.log('-------------value---------', value)
    const sql = await sqlQuery()

    const rows = sql.execute(
      'UPDATE todoTable SET `value` = ?,`status` = ?  WHERE `key`  = ?',
      [value, status, key]
    )

    res.status(200).json(rows)
  } catch (e: any) {
    res.status(500).json({ message: e.message })
  }
}
