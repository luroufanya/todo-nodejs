import sqlQuery from '../../until/connect'

export default async function add(req: any, res: any) {
  try {
    const { value } = req?.body

    const sql = await sqlQuery()
    const key = Math.floor(Math.random() * 100000)
    const rows = sql.execute(
      'insert into `todoTable` (`key`,`value`) values (?,?)',
      [key, value]
    )
    res.status(200).json(rows)
  } catch (e: any) {
    res.status(500).json({ message: e.message })
  }
}
