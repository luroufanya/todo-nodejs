import sqlQuery from '../../until/connect'

export default async function add(req: any, res: any) {
  try {
    const { key } = req?.body

    const sql = await sqlQuery()
    const rows = sql.execute('delete from `todoTable` where `key` = ? ', [key])
    res.status(200).json(rows)
  } catch (e: any) {
    res.status(500).json({ message: e.message })
  }
}
