import sqlQuery from '../../until/connect'

export default async function getData(req: any, res: any) {
  try {
    const sql = await sqlQuery()
    console.log('req------------', req.value)

    const [rows] = await sql.query('SELECT * FROM todoTable')
    res.status(200).json(rows)
  } catch (e: any) {
    res.status(500).json({ message: e.message })
  }
}
