import react, { useRef, useState } from 'react'
import { List, Input, Button, Row, Col, Checkbox, Form, Typography } from 'antd'
import {
  PlusOutlined,
  DeleteOutlined,
  EditOutlined,
  CheckOutlined,
  CloseOutlined,
} from '@ant-design/icons'
import { useRequest } from 'ahooks'
import webQuery from '../until/axios'

// 此函数在构建时被调用
export async function getServerSideProps() {
  // 调用外部 API 获取博文列表
  const initList = await webQuery.get({ url: 'api/getData' })

  // 通过返回 { props: { posts } } 对象，Blog 组件
  // 在构建时将接收到 `posts` 参数
  return {
    props: {
      initList,
    },
  }
}
type itemType = {
  value?: string
  key: string
  status: boolean
}

const renderTips = (
  <>
    <h1>getServerSideProps 服务端渲染 </h1>
    <h2>
      服务器端渲染： HTML 是在 每个页面请求
      时生成的。要设置某个页面使用服务器端渲染，请导出（export）
      getServerSideProps
      函数。由于服务器端渲染会导致性能比“静态生成”慢，因此仅在绝对必要时才使用此功能。
    </h2>
  </>
)
function Blog({ initList }: { initList: itemType[] }) {
  // Render posts...
  const [form] = Form.useForm<{ value: string }>()
  const [list, setPosts] = useState(initList)
  const [currEditItem, setCurrEditItem] = useState<itemType>()
  const inputRef = useRef(null)
  // const { runAsync: runAdd } = useRequest(
  //   reqPrams =>
  //     webQuery.post({
  //       url: 'api/add',
  //       body: reqPrams,
  //     }),
  //   {
  //     manual: true,
  //     onSuccess: () => {
  //       message.success('添加成功')
  //     },
  //   }
  // )
  const update = async () => {
    const newList = await webQuery.get({
      url: `api/getData`,
    })
    setPosts(newList)
  }

  const addItem = async () => {
    if (await form.validateFields()) {
      const data = form.getFieldsValue(true)
      // runAdd(data)
      const res = await webQuery.post({
        url: 'api/add',
        body: data,
      })
      form.resetFields()
      update()
    }
  }

  const deleteItem = async (key: string) => {
    const aa = await webQuery.post({
      url: 'api/delete',
      body: {
        key,
      },
    })
    update()
  }
  const editItem = async (params: itemType) => {
    const aa = await webQuery.post({
      url: 'api/update',
      body: params,
    })
    await update()
    setCurrEditItem(undefined)
  }
  const changeToEdit = async (item: itemType) => {
    console.log(item)

    await setCurrEditItem(item)
    const a = inputRef.current as any
    a.focus({
      cursor: 'all',
    })
  }
  const renderHeader = () => (
    <Row justify="center" align="middle">
      <Col span={10}>
        <Form form={form}>
          <Form.Item noStyle name="value" rules={[{ required: true }]}>
            <Input />
          </Form.Item>
        </Form>
      </Col>
      <Button icon={<PlusOutlined />} onClick={() => addItem()} />
    </Row>
  )

  return (
    <div
      style={{
        width: '70%',
        margin: 'auto',
      }}
      // onClick={() => currEditItem && setCurrEditItem(undefined)}
    >
      <h2>Data from MySQL:</h2>
      <List
        size="large"
        header={renderHeader()}
        bordered
        dataSource={list}
        renderItem={(item, i) => (
          <List.Item onDoubleClick={() => changeToEdit(item)}>
            <Row align="middle" style={{ width: '100%' }} wrap={false}>
              <Col span={2} xs={{ span: 2 }}>
                {i + 1}
              </Col>
              <Col flex={1}>
                {currEditItem?.key === item.key ? (
                  <Input.TextArea
                    autoSize
                    ref={inputRef}
                    value={currEditItem.value}
                    onChange={e => {
                      setCurrEditItem({
                        ...currEditItem,
                        value: e.target.value,
                      })
                    }}
                  />
                ) : (
                  <Typography.Text ellipsis={{ tooltip: item.value }}>
                    {item.value}
                  </Typography.Text>
                )}
              </Col>
              <Col
                xs={{ span: 4 }}
                style={{
                  display: 'flex',
                  gap: '8px',
                  justifyContent: 'center',
                }}
              >
                {currEditItem?.key === item.key ? (
                  <>
                    <Button
                      icon={<CheckOutlined />}
                      onClick={() => editItem(currEditItem)}
                    />
                    <Button
                      icon={<CloseOutlined />}
                      onClick={() => setCurrEditItem(undefined)}
                    />
                  </>
                ) : (
                  <>
                    <Button
                      icon={<EditOutlined />}
                      onClick={() => changeToEdit(item)}
                    />
                    <Button
                      icon={<DeleteOutlined />}
                      onClick={() => deleteItem(item.key)}
                    />
                    <Checkbox
                      checked={!!item?.status}
                      onClick={() =>
                        editItem({ ...item, status: !item?.status })
                      }
                    />
                  </>
                )}
              </Col>
            </Row>
          </List.Item>
        )}
      />
      {renderTips}
    </div>
  )
}

export default Blog
