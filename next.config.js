/** @type {import('next').NextConfig} */
const nextConfig = {
  env: {
    // DB_HOST: process.env.DB_HOST || '47.108.136.163',
    DB_HOST: process.env.DB_HOST || '127.0.0.1',
    DB_USER: process.env.DB_USER || 'root',
    DB_PASSWORD: process.env.DB_PASSWORD || 'qwer1234',
    DB_DATABASE: process.env.DB_DATABASE || 'todoDataBase',
  },
}

module.exports = nextConfig
